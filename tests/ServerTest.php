<?php

namespace Tests\Reanmachine\MiddlewareKata;

use Reanmachine\MiddlewareKata\Http\Request;

class ServerTest extends KataTestCase
{
    public function testBadAuthIsUnauthorized()
    {
        $request = Request::get('/', [], ['Authentication' => 'Bearer bad']);

        $response = $this->getServer()->process($request);

        $this->assertUnauthorized($response);
        $this->assertLoggedError('Invalid identity token.');
    }

    public function testLogsRequest()
    {
        $request = Request::get('/somewhere/missing', ['a' => 10, 'b' => 'other']);
        $requestPost = Request::post('/somewhere/missing', ['a' => 10, 'b' => 'other']);
        $requestOk = Request::get('/', ['a' => 10, 'b' => 'other']);

        $server = $this->getServer();

        $server->process($request);
        $server->process($requestPost);
        $server->process($requestOk);

        $this->assertLoggedInfo('[Request] GET /somewhere/missing, Query: {"a":10,"b":"other"}, Result: 404');
        $this->assertLoggedInfo('[Request] POST /somewhere/missing, Result: 404');
        $this->assertLoggedInfo('[Request] GET /, Query: {"a":10,"b":"other"}, Result: 200');
    }

    public function testAnonymousUserCanAccessHome()
    {
        $request = Request::get('/');
        $response = $this->getServer()->process($request);

        $this->assertOk($response);
        $this->assertContent('Hello, Guest. Welcome to the Kata.', $response);
    }

    public function testAuthenticatedUserCanAccessHomeWithPersonalization()
    {
        $request = Request::get('/', [], ['Authentication' => 'Bearer for-kata-god']);

        $response = $this->getServer()->process($request);

        $this->assertOk($response);
        $this->assertContent('Hello, Kata God. Welcome to the Kata.', $response);
    }

    public function testSessionDataFetched()
    {
        $requestGod = Request::get('/notifications', [], ['Authentication' => 'Bearer for-kata-god']);
        $requestNotify = Request::get('/notifications', [], ['Authentication' => 'Bearer for-notify']);

        $server = $this->getServer();
        $responseGod = $server->process($requestGod);
        $responseNotify = $server->process($requestNotify);

        $this->assertOk($responseGod);
        $this->assertContent('{"id":"god","notifications_enabled":false}', $responseGod);

        $this->assertOk($responseNotify);
        $this->assertContent('{"id":"notify","notifications_enabled":true}', $responseNotify);
    }

    public function testClaims()
    {
        $requestGod = Request::get('/claims', [], ['Authentication' => 'Bearer for-kata-god']);
        $requestNotify = Request::get('/claims', [], ['Authentication' => 'Bearer for-notify']);
        $requestNobody = Request::get('/claims', [], []);

        $server = $this->getServer();
        $responseGod = $server->process($requestGod);
        $responseNotify = $server->process($requestNotify);
        $responseNobody = $server->process($requestNobody);

        $this->assertOk($responseGod);
        $this->assertContent('Hello. Katas are wonderful. With Godly Power!', $responseGod);

        $this->assertOk($responseNotify);
        $this->assertContent('Hello. Katas are wonderful.', $responseNotify);

        $this->assertOk($responseNobody);
        $this->assertContent('Hello.', $responseNobody);
    }

    public function testForbidden()
    {
        $requestGod = Request::get('/god/ego', [], ['Authentication' => 'Bearer for-kata-god']);
        $requestNotify = Request::get('/god/ego', [], ['Authentication' => 'Bearer for-notify']);

        $server = $this->getServer();
        $responseGod = $server->process($requestGod);
        $responseNotify = $server->process($requestNotify);

        $this->assertOk($responseGod);
        $this->assertContent('We are kata gods!', $responseGod);

        $this->assertForbidden($responseNotify);
    }
}