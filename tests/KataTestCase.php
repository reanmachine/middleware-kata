<?php

namespace Tests\Reanmachine\MiddlewareKata;

use PHPUnit\Framework\TestCase;
use Reanmachine\MiddlewareKata\Http\Response;
use Reanmachine\MiddlewareKata\Server;

class KataTestCase extends TestCase
{
    private CollectingLogger $logger;

    protected function getServer(): Server
    {
        $this->logger = new CollectingLogger();
        return new Server($this->logger);
    }

    protected function assertOk(Response $response)
    {
        $this->assertEquals(200, $response->getStatusCode());
    }

    protected function assertUnauthorized(Response $response)
    {
        $this->assertEquals(401, $response->getStatusCode());
    }

    protected function assertForbidden(Response $response)
    {
        $this->assertEquals(403, $response->getStatusCode());
    }

    protected function assertContent(string $expected, Response $response)
    {
        $this->assertEquals($expected, $response->getContent());
    }

    protected function assertLoggedError(string $error)
    {
        $this->assertContains(
            $error,
            $this->logger->getLevelMessages('error')
        );
    }

    protected function assertLoggedInfo(string $message)
    {
        $this->assertContains(
            $message,
            $this->logger->getLevelMessages('info')
        );
    }
}