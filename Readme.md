# Middleware Kata

This is a refactoring kata to demonstrate the power of a chain of responsibility pattern by building a middleware pattern for a ficticious http processing server.

Do not Change anything in:
* `src/Http/*`
* `src/Services/*`

Do not Change the signature of (but feel free to change the implementation)
* `src/Server.php`

There are tests to verify the behavior, run them with `composer test`.

