<?php

namespace Reanmachine\MiddlewareKata\Handlers;

use Reanmachine\MiddlewareKata\Http\Context;
use Reanmachine\MiddlewareKata\Http\Request;
use Reanmachine\MiddlewareKata\Http\RequestHandler;
use Reanmachine\MiddlewareKata\Http\Response;

class GodHandler implements RequestHandler
{
    public function handle(Context $context, Request $request): Response
    {
        return new Response(200, 'We are kata gods!');
    }
}