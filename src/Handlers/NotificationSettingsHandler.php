<?php

namespace Reanmachine\MiddlewareKata\Handlers;

use Reanmachine\MiddlewareKata\Http\Context;
use Reanmachine\MiddlewareKata\Http\JsonResponse;
use Reanmachine\MiddlewareKata\Http\Request;
use Reanmachine\MiddlewareKata\Http\RequestHandler;
use Reanmachine\MiddlewareKata\Http\Response;

class NotificationSettingsHandler implements RequestHandler
{
    public function handle(Context $context, Request $request): Response
    {
        $session = $context->getData('session') ?? [];
        $notifications = $session['notifications'] ?? false;

        return new JsonResponse([
            'id' => $context->getIdentity()->getIdentifier(),
            'notifications_enabled' => $notifications
        ]);
    }
}