<?php

namespace Reanmachine\MiddlewareKata\Handlers;

use Reanmachine\MiddlewareKata\Http\Context;
use Reanmachine\MiddlewareKata\Http\Request;
use Reanmachine\MiddlewareKata\Http\RequestHandler;
use Reanmachine\MiddlewareKata\Http\Response;

class HomepageHandler implements RequestHandler
{
    public function handle(Context $context, Request $request): Response
    {
        $name = $context->getIdentity()->getName();

        return new Response(200, "Hello, $name. Welcome to the Kata.");
    }
}