<?php

namespace Reanmachine\MiddlewareKata\Handlers;

use Reanmachine\MiddlewareKata\Http\Context;
use Reanmachine\MiddlewareKata\Http\Request;
use Reanmachine\MiddlewareKata\Http\RequestHandler;
use Reanmachine\MiddlewareKata\Http\Response;

class ClaimsHandler implements RequestHandler
{
    public function handle(Context $context, Request $request): Response
    {
        $identity = $context->getIdentity();

        $response = 'Hello.';

        if (in_array('does-katas', $identity->getClaims())) {
            $response .= ' Katas are wonderful.';
        }

        if (in_array('is-a-god', $identity->getClaims())) {
            $response .= ' With Godly Power!';
        }

        return new Response(200, $response);
    }
}