<?php

namespace Reanmachine\MiddlewareKata\Http;

/**
 * Represents a context for the lifecycle of the request processing, where context to the
 * request can be stored.
 */
class Context
{
    private array $data;
    private Identity $identity;

    public function __construct()
    {
        $this->data = [];
        $this->identity = new AnonymousIdentity();
    }

    public function getData(string $key)
    {
        if (!isset($this->data[$key]))
        {
            throw new \Exception('Unknown Data: '.$key);
        }

        return $this->data[$key];
    }

    public function setData(string $key, $service)
    {
        $this->data[$key] = $service;
    }

    public function getIdentity()
    {
        return $this->identity;
    }

    public function setIdentity($identity): void
    {
        $this->identity = $identity;
    }
}