<?php

namespace Reanmachine\MiddlewareKata\Http;

class Identity
{
    private string $identifier;
    private string $name;
    private array $claims;

    public function __construct(string $identifier, string $name, array $claims)
    {
        $this->identifier = $identifier;
        $this->name = $name;
        $this->claims = $claims;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getClaims(): array
    {
        return $this->claims;
    }
}