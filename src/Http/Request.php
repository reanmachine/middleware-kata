<?php

namespace Reanmachine\MiddlewareKata\Http;

class Request
{
    private string $method;
    private string $path;
    private array $headers;
    private array $params;
    private array $form;
    private string $body;

    public static function get(string $path, array $params = [], array $headers = [])
    {
        return new Request([
            'path' => $path,
            'params' => $params,
            'headers' => $headers
        ]);
    }

    public static function post(string $path, array $form = [], array $headers = [])
    {
        return new Request([
            'method' => 'POST',
            'path' => $path,
            'form' => $form,
            'headers' => $headers
        ]);
    }

    public function __construct(array $data)
    {
        $this->method = $data['method'] ?? 'GET';
        $this->path = $data['path'] ?? '';
        $this->headers = $data['headers'] ?? [];
        $this->params = $data['params'] ?? [];
        $this->form = $data['form'] ?? [];
        $this->body = $data['body'] ?? '';
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getHeader(string $key, $default = null)
    {
        return $this->headers[$key] ?? $default;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function getForm(): array
    {
        return $this->form;
    }

    public function getBody(): string
    {
        return $this->body;
    }
}