<?php

namespace Reanmachine\MiddlewareKata\Http;

class ForbiddenResponse extends Response
{
    public function __construct()
    {
        parent::__construct(403, 'Resource access is forbidden.');
    }
}