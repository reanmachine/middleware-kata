<?php

namespace Reanmachine\MiddlewareKata\Http;

interface RequestHandler
{
    /**
     * Handle a request.
     *
     * @param Context $context
     *   The context for this request that might be relevant.
     * @param Request $request
     *   The request object.
     * @return Response
     *   A response object indicating how to produce a result.
     */
    public function handle(Context $context, Request $request): Response;
}