<?php

namespace Reanmachine\MiddlewareKata\Http;

class UnauthorizedResponse extends Response
{
    public function __construct()
    {
        parent::__construct(401, 'Unauthorized.');
    }
}