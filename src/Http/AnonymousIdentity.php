<?php

namespace Reanmachine\MiddlewareKata\Http;

class AnonymousIdentity extends Identity
{
    public function __construct()
    {
        parent::__construct('', 'Guest', []);
    }
}