<?php

namespace Reanmachine\MiddlewareKata\Http;

class NotFoundResponse extends Response
{
    public function __construct(string $message = 'Unable to find route.')
    {
        parent::__construct(404, $message);
    }
}