<?php

namespace Reanmachine\MiddlewareKata\Http;

class Response
{
    private int $statusCode;
    private array $headers;
    private string $content;

    public function __construct(int $statusCode, string $content = '', array $headers = [])
    {
        $this->statusCode = $statusCode;
        $this->headers = $headers;
        $this->content = $content;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}