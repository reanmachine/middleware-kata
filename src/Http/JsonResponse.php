<?php

namespace Reanmachine\MiddlewareKata\Http;

class JsonResponse extends Response
{
    public function __construct(array $payload, int $status = 200, $pretty = false)
    {
        $json = $pretty ? json_encode($payload, JSON_PRETTY_PRINT) : json_encode($payload);

        parent::__construct($status, $json);
    }
}