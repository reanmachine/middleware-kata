<?php

namespace Reanmachine\MiddlewareKata;

use Psr\Log\LoggerInterface;
use Reanmachine\MiddlewareKata\Handlers\ClaimsHandler;
use Reanmachine\MiddlewareKata\Handlers\GodHandler;
use Reanmachine\MiddlewareKata\Handlers\HomepageHandler;
use Reanmachine\MiddlewareKata\Handlers\NotificationSettingsHandler;
use Reanmachine\MiddlewareKata\Handlers\PrettyPrintHandler;
use Reanmachine\MiddlewareKata\Http\Context;
use Reanmachine\MiddlewareKata\Http\ForbiddenResponse;
use Reanmachine\MiddlewareKata\Http\IdentityException;
use Reanmachine\MiddlewareKata\Http\NotFoundResponse;
use Reanmachine\MiddlewareKata\Http\Request;
use Reanmachine\MiddlewareKata\Http\RequestHandler;
use Reanmachine\MiddlewareKata\Http\Response;
use Reanmachine\MiddlewareKata\Http\UnauthorizedResponse;
use Reanmachine\MiddlewareKata\Services\IdentityService;
use Reanmachine\MiddlewareKata\Services\SessionService;

/**
 * The http processing kernel. This is where all the fun happens.
 */
class Kernel implements RequestHandler
{
    private IdentityService $identityService;
    private SessionService $sessionService;
    private LoggerInterface $logger;

    public function __construct(IdentityService $identityService, SessionService $sessionService, LoggerInterface $logger)
    {
        $this->identityService = $identityService;
        $this->sessionService = $sessionService;
        $this->logger = $logger;
    }

    public function handle(Context $context, Request $request): Response
    {
        $auth = $request->getHeader('Authentication', '');

        if (str_starts_with($auth, 'Bearer ')) {
            $token = explode(' ', $auth)[1];

            try {
                $identity = $this->identityService->getIdentityFromToken($token);
                $context->setIdentity($identity);
            } catch (IdentityException $ex) {
                $this->logger->error('Invalid identity token.', ['exception' => $ex]);
                return new UnauthorizedResponse();
            }
        }

        $session = $this->sessionService->getSession($context->getIdentity());
        $context->setData('session', $session);

        if (str_starts_with($request->getPath(), '/god/') && !in_array('is-a-god', $context->getIdentity()->getClaims())) {
            $response = new ForbiddenResponse();
        } else {
            $response = $this->route($context, $request);
        }

        $this->logIt($request, $response);

        return $response;
    }

    private function logIt(Request $request, Response $response)
    {
        $method = $request->getMethod();
        $path = $request->getPath();

        $query = empty($request->getParams())
            ? null
            : json_encode($request->getParams());

        $code = $response->getStatusCode();

        if (is_null($query)) {
            $this->logger->info("[Request] $method $path, Result: $code");
        } else {
            $this->logger->info("[Request] $method $path, Query: $query, Result: $code");
        }
    }

    private function route(Context $context, Request $request): Response
    {
        return match ($request->getPath()) {
            '/' => (new HomepageHandler())->handle($context, $request),
            '/notifications' => (new NotificationSettingsHandler())->handle($context, $request),
            '/claims' => (new ClaimsHandler())->handle($context, $request),
            '/god/ego' => (new GodHandler())->handle($context, $request),
            default => new NotFoundResponse('Unable to found route for ' . $request->getPath()),
        };
    }
}