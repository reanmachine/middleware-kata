<?php

namespace Reanmachine\MiddlewareKata;

use Psr\Log\LoggerInterface;
use Reanmachine\MiddlewareKata\Http\Context;
use Reanmachine\MiddlewareKata\Http\Request;
use Reanmachine\MiddlewareKata\Http\Response;
use Reanmachine\MiddlewareKata\Services\IdentityService;
use Reanmachine\MiddlewareKata\Services\SessionService;

/**
 * The root of the kata, don't change the signature of this class, but change the insides as needed.
 */
class Server
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function process(Request $request): Response
    {
        $kernel = new Kernel(new IdentityService(), new SessionService(), $this->logger);
        $context = new Context();

        return $kernel->handle($context, $request);
    }
}