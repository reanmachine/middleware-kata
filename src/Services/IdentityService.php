<?php

namespace Reanmachine\MiddlewareKata\Services;

use Reanmachine\MiddlewareKata\Http\Identity;
use Reanmachine\MiddlewareKata\Http\IdentityException;

class IdentityService
{
    private Identity $identityKataGod;
    private Identity $identityNotify;

    public function __construct()
    {
        $this->identityKataGod = new Identity('god', 'Kata God', ['is-a-god', 'does-katas']);
        $this->identityNotify = new Identity('notify', 'Notification User', ['likes-notifications', 'does-katas']);
    }

    public function getIdentityFromToken(string $token)
    {
        return match ($token) {
            'for-kata-god' => $this->identityKataGod,
            'for-notify' => $this->identityNotify,
            default => throw new IdentityException('No identity by token ' . $token),
        };
    }
}