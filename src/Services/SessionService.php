<?php

namespace Reanmachine\MiddlewareKata\Services;

use Reanmachine\MiddlewareKata\Http\Identity;

class SessionService
{
    public function getSession(Identity $identity)
    {
        return match ($identity->getIdentifier()) {
            'god' => [
                'notifications' => false,
                'pretty_print' => false,
            ],
            'notify' => [
                'notifications' => true,
                'pretty_print' => false,
            ],
            'pretty' => [
                'notifications' => false,
                'pretty_print' => true,
            ],
            default => [],
        };
    }
}